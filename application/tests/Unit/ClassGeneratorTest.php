<?php

namespace Tests\Unit;

use App\Generator\ClassGenerate;
use Tests\TestCase;

class ClassGeneratorTest extends TestCase
{
    /**
     * @test
     */
    public function it_checks_that_the_infrastructure_is_stable()
    {
        //Given
        $classesGenerator = new ClassGenerate();

        $classesGenerator->generate('simple', 'featureName');
        //When

        //Then
    }

    /**
     * @test
     */
    public function it_checks_that_the_right_template_is_parsed()
    {
        //Given

        //When

        //Then
    }

    /**
     * @test
     */
    public function it_checks_that_that_the_right_classes_are_created()
    {
        //Given

        //When

        //Then
    }
}
