<?php

namespace Tests\Unit;

use App\Generator\TemplateParser;
use RecursiveIteratorIterator;
use Tests\TestCase;

class TemplateParserTest extends TestCase
{
    /**
     * @test
     */
    public function it_checks_that_a_template_directories_are_created()
    {
        //Given
        $feature = 'app/Features/featureName';
        $templateParser = new TemplateParser();

        //When
        if (file_exists($feature) && is_dir($feature)) {
            # https://stackoverflow.com/questions/1653771/how-do-i-remove-a-directory-that-is-not-empty
            $dir = new \RecursiveDirectoryIterator($feature, \RecursiveDirectoryIterator::SKIP_DOTS);

            foreach (new RecursiveIteratorIterator($dir, RecursiveIteratorIterator::CHILD_FIRST) as $filename => $file) {
                if (is_file($filename))
                    unlink($filename);
                else
                    rmdir($filename);
            }
            rmdir($feature); // Now remove myfolder
        }

        $templateParser
            ->setTemplate()
            ->setFeatureName('featureName')
            ->setOptions(['with' => ['repositories', 'contracts']])
            ->importTemplateFile()
            ->setupRootDirectory()
            ->generateFeatureDirectories();

        //Then
        $this->assertNotEmpty($templateParser->directories);
        $this->assertEquals(count($templateParser->getDirectories()), count($templateParser->generatedDirectories));
    }


}
