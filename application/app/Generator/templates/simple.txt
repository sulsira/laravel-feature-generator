directories[
    http/routes
    http/controllers
    http/resources
    database/models
    services
    Exceptions
    providers
]

classes[
    http/routes/api.php
    http/routes/routes.php
    http/routes/console.php
    http/controllers/featuresController.php extends controller
    http/controllers/featuresPingController.php extends controller
    http/resources/featuresResource.php extend resource
    database/models/feature.php extends model
    services/featureService.php
    Exceptions/featureException.php extends exception
    providers/featureServiceProvider.php extends serviceProvider
]
