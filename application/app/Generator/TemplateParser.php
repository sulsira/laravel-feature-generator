<?php

namespace App\Generator;

/**
 * TODO: THIS SHOULD BE BE A DIRECTORY GENERATOR IT TAKES IN A DIRECTORY TEMPLATE AND PARSE IT INTO AN ACTUAL DIRECTORY
 * TODO: THIS SHOULD IMPLEMENT THE ADOPTER BECAUSE IT SWITCH OUT DIFFERENCE DIRECTORY TEMPLATE EG, SIMPLE AND COMPLEX
 * Class TemplateParser
 * @package App\Generator
 */
class TemplateParser
{
    /**
     * @var string
     */
    public $templateName = 'simple';

    /**
     * @var array
     */
    public $directories = [];

    /**
     * @var
     */
    public $generatedDirectories;

    /**
     * @var
     */
    private $featureName;


    /**
     * @var
     */
    private $options;

    /**
     * @var
     */
    private $classes;

    /**
     * @var string
     */
    private $rootDirectory = 'app/Features';

    /**
     * Sets a template.
     *
     * @param string $templateName
     * @return TemplateParser
     */
    public function setTemplate($templateName = 'simple')
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * Sets a feature name.
     *
     * @param $featureName
     * @return TemplateParser
     */
    public function setFeatureName($featureName)
    {
        $this->featureName = ucfirst($featureName);

        return $this;
    }

    /**
     * Sets in the options.
     *
     * @param array $options
     * @return TemplateParser
     */
    public function setOptions(array $options = [])
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Gets all the directories created.
     *
     */
    public function getDirectories()
    {
        return $this->directories;
    }

    /**
     * Imports template file.
     *
     * @return string
     */
    public function importTemplateFile()
    {
        $file = require __DIR__ . '/templates/' . $this->templateName . '.template.php';

        $this->directories = $file['directories'];
        $this->classes = $file['classes'];

        return $this;
    }

    /**
     * Gets the template in scope.
     *
     * @return string
     */
    public function getTemplateName()
    {
        return $this->templateName;
    }

    /**
     * Generate feature directories.
     *
     * @throws \Exception
     */
    public function generateFeatureDirectories()
    {
        if (!$this->featureName) {
            throw new \Exception('Cannot create directories without features');
        }

        if (is_dir($this->rootDirectory) AND file_exists($this->rootDirectory)) {
            $this->makeDirectories($this->rootDirectory);
        }

        return $this;
    }

    /**
     * Creates directories.
     *
     * @param $rootDirectory
     * @throws \Exception
     */
    public function makeDirectories($rootDirectory)
    {
        foreach ($this->directories as $dir) {
            $directory = $rootDirectory . DIRECTORY_SEPARATOR . ucfirst($dir);

            mkdir($directory, 0755, true);

            if (file_exists($directory) AND is_dir($directory)) {
                $this->generatedDirectories[] = $directory;
            }
        }
    }

    /**
     * Finds or creates the root directory.
     *
     * @return string|null
     * @throws \Exception
     */
    public function setupRootDirectory()
    {
        $featureDirectory = $this->rootDirectory . DIRECTORY_SEPARATOR . $this->featureName;

        if (file_exists($featureDirectory) AND is_dir($featureDirectory) AND is_writable($featureDirectory)) {
            throw new \Exception('This feature already exist');
        }

        mkdir($featureDirectory, 0755, true);

        $this->rootDirectory = $featureDirectory;

        return $this;
    }
}
