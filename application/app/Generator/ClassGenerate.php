<?php

namespace App\Generator;

use App\Generator\Contracts\GeneratorInterface;

class ClassGenerate implements GeneratorInterface
{
    /**
     * @var
     */
    private $template;

    /**
     * @var
     */
    private $feature;

    /**
     * From a templates generates directories or classes.
     *
     * @param $template
     * @param $feature
     * @return mixed
     */
    public function generate($template, $feature)
    {
        $file = require __DIR__ . '/templates/simple.template.php';

        $this->template = $file['classes'];
        $this->feature = $feature;

        return $this->parseClassesWithStubs();
    }

    /**
     * Parse classes with stubs.
     *
     */
    private function parseClassesWithStubs()
    {
        foreach ($this->template as $file) {
            // import stub for the specfic class
            $this->parseClass($file);
        }
    }

    /**
     * Parse a class from a stub.
     *
     * @param $class
     * @param $stub
     */
    private function parseClass($class)
    {
        // find the stub

        // rebase
    }

    /**
     * Creates a new class from a parsed class.
     *
     * @param $parsedClass
     */
    private function generateClass($parsedClass)
    {

    }
}
