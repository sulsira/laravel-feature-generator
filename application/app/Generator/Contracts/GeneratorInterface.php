<?php

namespace App\Generator\Contracts;

/**
 * Interface GeneratorInterface
 * @package App\Generator\Contracts
 */
interface GeneratorInterface
{
    /**
     * From a templates generates directories or classes.
     *
     * @param $template
     * @param $feature
     * @return mixed
     */
    public function generate($template, $feature);
}
